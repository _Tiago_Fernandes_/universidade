import json
import matplotlib.pyplot as plt
from tkinter import filedialog
from tkinter import Tk
import subprocess
import http.server
import socketserver
import sys
import webbrowser
import threading
import os

def start_server():
    # Defina o diretório de onde servir os arquivos
    if getattr(sys, 'frozen', False):
        os.chdir(sys._MEIPASS)  # Muda para o diretório temporário do PyInstaller

    class Handler(http.server.SimpleHTTPRequestHandler):
        def __init__(self, *args, **kwargs):
            # Quando rodando como executável, serve os arquivos do diretório de trabalho atual.
            super().__init__(*args, directory=os.getcwd(), **kwargs)

    with socketserver.TCPServer(("", 8080), Handler) as httpd:
        print("Serving at port 8080")
        httpd.serve_forever()

# Caminho para o diretório onde os arquivos embutidos são extraídos
if getattr(sys, 'frozen', False):
    application_path = sys._MEIPASS  # Se estiver rodando como um executável PyInstaller
else:
    application_path = os.path.dirname(os.path.abspath(__file__))  # Em desenvolvimento

def combined_function():
    # --- Part 1: Functionality from script_1.py ---
    # Define the path for the original JSON file
    root = Tk()
    root.withdraw()  # Hide the Tkinter root window
    file_path = filedialog.askopenfilename(title="Select JSON file", filetypes=[("JSON files", "*.json")])
    if not file_path:
        print("No file selected.")
        return

    # Read the original JSON file
    with open(file_path, 'r') as file:
        original_json_data = json.loads(file.read())

    # Find the index where 'userLog' first appears
    userlog_index = next((i for i, entry in enumerate(original_json_data) if 'userLog' in entry), None)

    # If 'userLog' is found, filter the data from that point onwards
    if userlog_index is not None:
        original_userlog_data = original_json_data[userlog_index:]
    else:
        original_userlog_data = []

    # Extract 'clickDetails' and 'keyLogger' from the userLog entries
    click_details = []
    keylogger = []

    for entry in original_userlog_data:
        if 'userLog' in entry:
            userlog = entry['userLog']
            if 'clicks' in userlog and 'clickDetails' in userlog['clicks']:
                click_details.extend(userlog['clicks']['clickDetails'])
            if 'keyLogger' in userlog:
                keylogger.extend(userlog['keyLogger'])

    # Combine and sort both clickDetails and keylogger by their timestamps
    combined_user_interactions = click_details + keylogger
    sorted_user_interactions = sorted(combined_user_interactions, key=lambda x: x['timestamp'])

    # Define the file path for the new JSON file
    new_json_file_path = 'sorted_user_interactions.json'

    # Write the sorted user interactions to the new JSON file
    with open(new_json_file_path, 'w') as new_file:
        json.dump(sorted_user_interactions, new_file, indent=4)

    # --- Part 2: Functionality from script_2.py ---
    # Read the sorted interactions JSON file
    with open(new_json_file_path, 'r') as file:
        sorted_interactions = json.load(file)

    # Define the initial structure of the tree and counters
    tree_data = {
        'name': 'root',
        'children': []
    }
    image_count = 0
    lecture_count = 0

    # Helper function to create a new page
    def create_new_page():
        return {'name': f"Page {len(tree_data['children']) + 1}", 'children': []}

    # Helper function to add interaction to the current page
    def add_interaction(current_page, interaction):
        nonlocal image_count, lecture_count

        # Check the interaction type and update counters accordingly
        if 'node' in interaction:
            # Se for um dos nodes que queremos excluir, retorne imediatamente sem adicionar
            if any(interaction['node'].startswith(tag) for tag in ['<div', '<button', '<input']):
                return

            if any(tag in interaction['node'] for tag in ['<img/', '<canvas']):
                image_count += 1
                interaction_name = "Auxiliar de imagem"
            elif any(tag in interaction['node'] for tag in ['<p>', '<strong>']):
                lecture_count += 1
                interaction_name = "Auxiliar de Leitura"
            else:
                interaction_name = interaction['node']
        elif 'data' in interaction:
            # Se for um keypress, também não adicionar
            return
        else:
            # Se não for nenhum dos acima, usar um nome genérico para a interação
            interaction_name = "Other Interaction"

        # Add the interaction to the current page
        current_page['children'].append({
            'name': interaction_name,
            'timestamp': interaction['timestamp'],
            'x': interaction.get('x'),
            'y': interaction.get('y')
        })

    # Initialize the first page
    current_page = create_new_page()
    tree_data['children'].append(current_page)

    # Process each interaction
    for interaction in sorted_interactions:
        # Check if the interaction is a click that should create a new page
        if 'node' in interaction and any(
                button in interaction['node'] for button in ['Continuar', 'Continue', 'Começar', 'Start', 'Begin']):
            current_page = create_new_page()
            tree_data['children'].append(current_page)
        else:
            add_interaction(current_page, interaction)

    # After processing all interactions, add the counts to the AUX
    tree_data['AUX'] = {
        'image_count': image_count,
        'lecture_count': lecture_count,
        'total_count': image_count + lecture_count
    }

    # No local onde você salva o JSON:
    if getattr(sys, 'frozen', False):
        # Se estiver rodando como um executável PyInstaller
        json_path = os.path.join(sys._MEIPASS, 'tree_structure_with_counts.json')
    else:
        json_path = 'tree_structure_with_counts.json'

    with open(json_path, 'w') as f:
        json.dump(tree_data, f, indent=4)


    # Caminho para a pasta 'documentos'
    documents_folder_path = os.path.join(os.getcwd(), 'documentos')
    if not os.path.exists(documents_folder_path):
        os.makedirs(documents_folder_path)

    # Salvar o arquivo auxiliar na pasta 'documentos'
    aux_json_path = os.path.join(documents_folder_path, os.path.basename(file_path).rsplit('.', 1)[0] + "_aux.json")
    with open(aux_json_path, 'w') as aux_file:
        json.dump(tree_data, aux_file, indent=4)

    aux_data = tree_data['AUX']

    # Extrair os valores e rótulos para o gráfico de pizza
    labels = ['Image Count', 'Lecture Count']
    sizes = [aux_data['image_count'], aux_data['lecture_count']]
    total = aux_data['total_count']

    # Cores para cada seção do gráfico de pizza
    colors = ['#ff9999', '#66b3ff']

    # Criar o gráfico de pizza
    fig, ax = plt.subplots()
    wedges, texts, autotexts = ax.pie(sizes, labels=labels, colors=colors, autopct=lambda pct: f'{pct:.1f}%',
                                      startangle=140)

    # Configuração para melhorar a legibilidade dos rótulos automáticos (percentagens)
    plt.setp(autotexts, size=10, weight="bold", color="white")

    # Título do gráfico
    ax.set_title('Distribution of Interactions')

    # Adicionar uma legenda com os valores absolutos e totais
    ax.legend(wedges, [f'{label}: {size} (Total: {total})' for label, size in zip(labels, sizes)], title="Counts",
              loc="center left", bbox_to_anchor=(1, 0, 0.5, 1))

    # Caminho para a pasta 'imagens'
    images_folder_path = os.path.join(os.getcwd(), 'imagens')
    if not os.path.exists(images_folder_path):
        os.makedirs(images_folder_path)

    # Salvar o gráfico na pasta 'imagens'
    graph_image_path = os.path.join(images_folder_path, os.path.basename(file_path).rsplit('.', 1)[0] + "_graf.png")
    plt.savefig(graph_image_path, format='png', bbox_inches='tight')

    # Mostrar o gráfico
    plt.show()

    thread = threading.Thread(target=start_server)
    thread.daemon = True
    thread.start()

    # Abrir automaticamente o navegador com o arquivo HTML
    webbrowser.open(f'http://localhost:8080/updated_index.html')

    print("Servidor iniciado. Pressione Ctrl+C para encerrar.")

    # Bloqueia o script principal enquanto a thread do servidor estiver ativa
    try:
        while True:
            pass
    except KeyboardInterrupt:
        print("Servidor interrompido pelo usuário.")
        # Aqui você pode adicionar qualquer limpeza necessária antes de sair
        sys.exit(0)

# Chame a função combined_function() para executar o script
combined_function()
