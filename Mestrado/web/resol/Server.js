var express = require ('express');
var app = express();

app.use(express.static('public'));

app.get('/', function(req, res) {
  res.send('PWA - MEI-UTAD - Exercicio HTML+CSS+JS');
});

var server = app.listen(8080, function() {
    var host = server.address().address;
    var port = server.address().port;
    
    console.log('Servidor', host, 'porta', port);
    });

