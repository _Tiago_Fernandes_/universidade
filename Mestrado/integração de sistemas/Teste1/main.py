import requests

# Inicialização da lista de registos
registos = []
contador_id = 1


# Função para mostrar o menu
def mostrar_menu():
    print("\nMenu:")
    print("1 - Inserir registo")
    print("2 - Mostrar todos os registos")
    print("3 - Mostrar registo por ID")
    print("4 - Alterar registo por ID")
    print("5 - Apagar registo por ID")
    print("6 - Sair")

    global contador_id


# Função para inserir um novo registo
def inserir_registo():
    url = "https://gorest.co.in/public/v2/users"  # Substitua pela URL correta da sua API
    token_de_autenticacao = 'e14bf46a93c5abfdbad19bae48395086cbf4d498d9be368b3036b0714a70e54c'
    headers = {
        'Authorization': f'Bearer {token_de_autenticacao}',
        'Content-Type': 'application/json'
    }
    dados = {
    "name": "Bizelazzzzz",
    "job": "Trolha",
    "email": "dhagfuhds@hdusad",
    "gender": "male",
    "status": "inactive"
    }

    # Fazendo a solicitação POST para a API
    resposta = requests.post(url, json=dados, headers=headers)

    if resposta.status_code == 200 or resposta.status_code == 201:
        print("Registo inserido com sucesso.")
    else:
        print(f"Erro ao inserir registo. Código de resposta: {resposta.status_code}")




def mostrar_todos():
    url = "https://gorest.co.in/public/v2/users"  # Substitua pela URL correta da sua API
    token_de_autenticacao = 'e14bf46a93c5abfdbad19bae48395086cbf4d498d9be368b3036b0714a70e54c'  # Substitua pelo seu token de autenticação real
    headers = {
        'Authorization': f'Bearer {token_de_autenticacao}'
    }

    # Fazendo a solicitação GET para a API
    resposta = requests.get(url, headers=headers)

    if resposta.status_code == 200:
        registros = resposta.json()  # Convertendo a resposta da API de JSON para uma lista de dicionários
        if not registros:
            print("Não há registros.")
            return
        for registro in registros:
            # Imprimindo os detalhes do registro. Adapte as chaves do dicionário conforme necessário
            print(f"ID: {registro['id']}, Nome: {registro['name']}, E-mail: {registro['email']}, Gênero: {registro['gender']}, Status: {registro['status']}")
    else:
        print(f"Erro ao buscar registros. Código de resposta: {resposta.status_code}")

# Função para mostrar um registo pelo ID
def mostrar_por_id():
    id_procurado = int(input("Digite o ID do registo: "))
    url = f"https://gorest.co.in/public/v2/users/{id_procurado}"
    token_de_autenticacao = 'e14bf46a93c5abfdbad19bae48395086cbf4d498d9be368b3036b0714a70e54c'
    headers = {
        'Authorization': f'Bearer {token_de_autenticacao}'
    }

    resposta = requests.get(url, headers=headers)

    if resposta.status_code == 200:
        try:
            registro = resposta.json()
            print(
                f"ID: {registro['id']}, Nome: {registro['name']}, E-mail: {registro['email']}, Gênero: {registro['gender']}, Status: {registro['status']}")
        except ValueError as e:  # Captura o erro se a resposta não puder ser convertida em JSON
            print(f"Erro ao decodificar JSON: {e}")
            print(f"Resposta bruta: {resposta.text}")  # Imprime a resposta bruta como texto
    else:
        print(f"Erro ao buscar registro. Código de resposta: {resposta.status_code}")


# Função para alterar um registo pelo ID
def alterar_registo():
    id_procurado = int(input("Digite o ID do registo a ser alterado: "))
    url = f"https://gorest.co.in/public/v2/users/{id_procurado}"
    token_de_autenticacao = 'e14bf46a93c5abfdbad19bae48395086cbf4d498d9be368b3036b0714a70e54c'
    headers = {
        'Authorization': f'Bearer {token_de_autenticacao}',
        'Content-Type': 'application/json'
    }

    # Solicita as atualizações para o registro
    nome = input("Digite o novo nome (deixe em branco para não alterar): ")
    email = input("Digite o novo e-mail (deixe em branco para não alterar): ")
    genero = input("Digite o novo gênero (male/female, deixe em branco para não alterar): ")
    status = input("Digite o novo status (active/inactive, deixe em branco para não alterar): ")

    # Constrói o dicionário de dados com as atualizações, excluindo campos vazios
    dados_atualizacao = {}
    if nome: dados_atualizacao['name'] = nome
    if email: dados_atualizacao['email'] = email
    if genero: dados_atualizacao['gender'] = genero
    if status: dados_atualizacao['status'] = status

    # Fazendo a solicitação PATCH para a API
    resposta = requests.patch(url, json=dados_atualizacao, headers=headers)

    if resposta.status_code in [200, 204]:
        print("Registro atualizado com sucesso.")
    else:
        print(f"Erro ao atualizar registro. Código de resposta: {resposta.status_code}, Mensagem: {resposta.text}")


# Função para apagar um registo pelo ID
def apagar_por_id():
    id_procurado = int(input("Digite o ID do registo a ser apagado: "))
    url = f"https://gorest.co.in/public/v2/users/{id_procurado}"  # Substitua pela URL correta da sua API
    token_de_autenticacao = 'e14bf46a93c5abfdbad19bae48395086cbf4d498d9be368b3036b0714a70e54c'  # Substitua pelo seu token de autenticação real
    headers = {
        'Authorization': f'Bearer {token_de_autenticacao}'
    }

    # Fazendo a solicitação DELETE para a API
    resposta = requests.delete(url, headers=headers)

    if resposta.status_code in [200, 204]:  # 200 OK ou 204 No Content são respostas comuns para uma solicitação DELETE bem-sucedida
        print("Registro apagado com sucesso.")
    else:
        print(f"Erro ao apagar registro. Código de resposta: {resposta.status_code}, Mensagem: {resposta.text}")


# Loop principal do programa
while True:
    mostrar_menu()
    opcao = input("Escolha uma opção: ")

    if opcao == "1":
        inserir_registo()
    elif opcao == "2":
        mostrar_todos()
    elif opcao == "3":
        mostrar_por_id()
    elif opcao == "4":
        alterar_registo()
    elif opcao == "5":
        apagar_por_id()
    elif opcao == "6":
        print("Saindo...")
        break
    else:
        print("Opção inválida. Tente novamente.")
