modes = ("notepad","wordpad")
option = select("Escolha a opção browser ou wordpad",options = modes)

if option == modes[0]:
    # Abre o Notepad
    type(Key.WIN)
    wait(1)
    type("txt")
    type(Key.ENTER)
    wait(3)  # Espera um pouco para a aplicação abrir
    type("O Notepad apenas permite escrever texto simples!")
    nome_arquivo = "FicheiroNotepad"

elif option == modes[1]:
    # Abre o WordPad
    type(Key.WIN)
    wait(1)
    type("wordpad")
    type(Key.ENTER)
    wait(3)  # Espera um pouco para a aplicação abrir
    type("Com o Wordpad consigo formatar o texto!")
    nome_arquivo = "FicheiroWordpad"


# Salva o arquivo
type("g", Key.CTRL)
wait(1)  # Espera o menu de salvar aparecer

type(nome_arquivo)
type(Key.ENTER)

wait(2)  # Espera um pouco para garantir que o arquivo foi salvo

# Fecha a aplicação
type(Key.F4, KeyModifier.ALT)