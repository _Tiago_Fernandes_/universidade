import * as THREE from 'three';
import {FBXLoader} from 'FBXLoader';
import { PointerLockControls } from 'PointerLockControls';
import { GLTFLoader } from '/node_modules/three/examples/jsm/loaders/GLTFLoader.js';
import { FontLoader } from '/node_modules/three/examples/jsm/loaders/FontLoader.js';

document.addEventListener('DOMContentLoaded', Start);

var cena = new THREE.Scene();

var renderer = new THREE.WebGLRenderer();


var textureLoader = new THREE.TextureLoader();
renderer.setSize(window.innerWidth - 15, window.innerHeight - 80);
renderer.setClearColor(0xaaaaaa);

function musicaCrash(){
const audioLoader = new THREE.AudioLoader();
audioLoader.load('/music/musicaCrash.mp3', function(buffer) {
  audio.setBuffer(buffer);
  audio.setLoop(true); // Defina como 'true' se desejar que a música seja reproduzida em loop
  //audio.play();
});
}

document.body.appendChild(renderer.domElement);

var geometriaCubo = new THREE.BoxGeometry(1, 1, 1);
var textura = new THREE.TextureLoader().load('./Images/boxImage.jpg');
var materialTextura = new THREE.MeshStandardMaterial({ map: textura });
var meshCubo = new THREE.Mesh(geometriaCubo, materialTextura);
var objetoImportado;
var importer = new FBXLoader();
meshCubo.translateZ(-6.0);
var objetoImportado;
var mixerAnimacao;
var relogio = new THREE.Clock();


//codigo crash
function CreateCrash(x,y,z){
    
    
    var loader = new FBXLoader();
    loader.setPath('./Crash5/');
    loader.load('Crash.fbx', (fbx) => {
        fbx.scale.setScalar(0.005);
        fbx.position.set(x, y, z);
        const pointLight = new THREE.PointLight(0xffffff, 2,10,0.5);
        pointLight.position.set(x, y+1, z);
        fbx.add(pointLight);
        fbx.traverse(c => {
            c.castShadow = true;
        });
        var mixer = new THREE.AnimationMixer(fbx);
        const anim = new FBXLoader();
        anim.setPath('./animation/');
        anim.load('walk.fbx', (anim) => {
            var idle = mixer.clipAction(anim.animations[0]);
            idle.play();
        });

        cena.add(fbx);
    });
}



//

//movimento do personagem
document.addEventListener("keydown", onDocumentKeyDown, false);
function onDocumentKeyDown(event){
    var keyCode = event.which;
    if(keyCode == 87){
        controls.moveForward(0.60)
    }
    else if (keyCode == 83){
        controls.moveForward(-0.60)
    }
    else if (keyCode == 65){
        controls.moveRight(-0.60)
    }
    else if (keyCode == 68){
        controls.moveRight(0.60)
    }
    else if (keyCode == 32){
        if(meshCubo.parent === cena){
            cena.remove(meshCubo);
        }else{
            cena.add(meshCubo)
        }
    }
};
//

//codigo do chao
var geometriaChao = new THREE.PlaneGeometry(300, 300); // geometria do chão, 10x10
var texturaChao = new THREE.TextureLoader().load('./Floor/floorcb2.png', function(texture){
    texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
    texture.minFilter = THREE.LinearMipmapLinearFilter;
    texture.anisotropy = 100;
    texture.repeat.set(10, 10);

    var materialChao = new THREE.MeshPhongMaterial({ map: texturaChao }); // material do chão
// textura do chão
var meshChao = new THREE.Mesh(geometriaChao, materialChao); // malha do chão
meshChao.rotation.x = -Math.PI / 2; // rotação para ficar no plano XZ
meshChao.position.y = 0; // posição abaixo do personagem
cena.add(meshChao); // adiciona o chão na cena
});
//

//codigo do ceu
var texture_dir = new THREE.TextureLoader().load('./Skybox/barren_rt.jpg');
var texture_esq = new THREE.TextureLoader().load('./Skybox/barren_lf.jpg');
var texture_up = new THREE.TextureLoader().load('./Skybox/barren_up.jpg');
var texture_dn = new THREE.TextureLoader().load('./Skybox/barren_dn.jpg');
var texture_bk = new THREE.TextureLoader().load('./Skybox/barren_bk.jpg');
var texture_ft = new THREE.TextureLoader().load('./Skybox/barren_ft.jpg');
var materialArray = [];
materialArray.push(new THREE.MeshBasicMaterial({ map: texture_dir }));
materialArray.push(new THREE.MeshBasicMaterial({ map: texture_esq }));
materialArray.push(new THREE.MeshBasicMaterial({ map: texture_up }));
materialArray.push(new THREE.MeshBasicMaterial({ map: texture_dn }));
materialArray.push(new THREE.MeshBasicMaterial({ map: texture_bk }));
materialArray.push(new THREE.MeshBasicMaterial({ map: texture_ft }));
for( var i = 0; i < 6; i++){
    materialArray[i].side = THREE.BackSide;}
var skyboxGeo = new THREE.BoxGeometry(200,200,200);
var skybox = new THREE.Mesh( skyboxGeo, materialArray);
cena.add(skybox);
//

//Codigo da caneca
// define the arc path points
// define the arc path points
var arcPoints = [];
for (var i = 0; i <= 20; i++) {
    var angle = (i / 20) * Math.PI - Math.PI / 2.7; // range from -90 to 90 degrees
    var x = Math.cos(angle) * 15; // radius of 5
    var y = Math.sin(angle) * 6; // vertical offset of 2 units
    var z = i - 10; // length of 20 units, centered at 0
    arcPoints.push(new THREE.Vector3(x, y, z));
}
// create a CatmullRomCurve3 with the arc points
var curve = new THREE.CatmullRomCurve3(arcPoints);
var textureLoader = new THREE.TextureLoader();
var texturaVidro = textureLoader.load('./assetsbeer/glass.jpg');
var texturaCerveja = textureLoader.load('./assetsbeer/beer.jpg');
var geometriaCopo = new THREE.CylinderGeometry(16, 16, 40);
var materialCopo = new THREE.MeshBasicMaterial({
    map: texturaVidro,
    //color:
    transparent: true,
    opacity: 0.5,
});
// Create a tube geometry using the curve
var geometriaAlca = new THREE.TubeGeometry(curve, 100, 2.5, 20, false);
var materialAlca = new THREE.MeshBasicMaterial({
    map: texturaVidro,
    //color: 
    transparent: true,
    opacity: 0.5,
});
// Create the beer
var geometriaCerveja = new THREE.CylinderGeometry(16, 16, 40);
var materialCerveja = new THREE.MeshBasicMaterial({
    map: texturaCerveja,
    //color: 
    transparent: true,
    opacity: 0.95,
});
var conjunto = new THREE.Object3D();
var meshCopo = new THREE.Mesh(geometriaCopo, materialCopo);
var meshAlca = new THREE.Mesh(geometriaAlca, materialAlca);
var meshCerveja = new THREE.Mesh(geometriaCerveja, materialCerveja);

conjunto.add(meshCopo);
conjunto.add(meshAlca);
conjunto.add(meshCerveja);
conjunto.scale.x = 0.005;
conjunto.scale.y = 0.005;
conjunto.scale.z = 0.005;
conjunto.position.x=7;
conjunto.position.y=1;
conjunto.position.z=-45;
cena.add(conjunto);
//

//codigo bar
const buildingTexture = textureLoader.load('assetsbar/moe_wall.png', function (texture) {
    texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
    texture.repeat.set(4, 4)});
//const awningTexture = textureLoader.load('assets/AwningRGB.png');
const windowTexture = textureLoader.load('assetsbar/moe_window.png');
const doorTexture = textureLoader.load('assetsbar/moe_door.png');

function createBuilding(posx, posy, posz) {
    const geometry = new THREE.BoxGeometry(10, 5, 8);
    const material = new THREE.MeshPhongMaterial({ map: buildingTexture});
    const building = new THREE.Mesh(geometry, material);
    building.position.set(posx, posy+2.5, posz);
    cena.add(building);
    return building;
}
function createMoesBarShape() {
  const shape = new THREE.Shape();
  shape.moveTo(0, 0);
  shape.lineTo(10, 0);
  shape.lineTo(10, 1);
  shape.lineTo(9, 2);
  shape.lineTo(1, 2);
  shape.lineTo(0, 1);
  shape.lineTo(0, 0);
  return shape;
}
function createMoesBarGeometry(shape) {
  const extrudeSettings = {
    steps: 1,
    depth: 0.5,
    bevelEnabled: false,
  };
  return new THREE.ExtrudeGeometry(shape, extrudeSettings);
}
function createMoesBar() {
  const shape = createMoesBarShape();
  const geometry = createMoesBarGeometry(shape);
  const material = new THREE.MeshPhongMaterial({ color: 0x3d294a });
  const moesBar = new THREE.Mesh(geometry, material);
 
  return moesBar;
}
function createSign(posx, posy, posz) {
    const geometry = new THREE.BoxGeometry(3, 1.5, 0.1);
    const material = new THREE.MeshPhongMaterial({ color: 0xbe98cb });
    const sign = new THREE.Mesh(geometry, material);
    sign.position.set(posx + 0, posy + 3.5, posz -4.01);
    cena.add(sign);
    return sign;
}
function createDoor(posx, posy, posz) {
    const geometry = new THREE.BoxGeometry(1.5, 2.5, 0.1);
    const material = new THREE.MeshPhongMaterial({ map: doorTexture });
    const door = new THREE.Mesh(geometry, material);
    door.position.set(posx + 0, posy + 1.25, posz -4);
    cena.add(door);
    return door;
}
function createlateralWindow(xposition, yposition, zposition) {
  const geometry = new THREE.BoxGeometry(0.1, 1.5, 2);
  const material = new THREE.MeshPhysicalMaterial({
      map: windowTexture,
      color: 0xffffff,
      transmission: 0.3,
      thickness: 1,
      roughness: 0.6,
      clearcoat: 1.0,
      clearcoatRoughness: 0.2,
      metalness: 0.3,
      transparent: true,
      });
  const window = new THREE.Mesh(geometry, material);
  //enableShadows(window);
  window.position.set(xposition, yposition, zposition);
  cena.add(window);
  return window;
}
function createWindow(xPosition, yPosition, zPosition) {
    const geometry = new THREE.BoxGeometry(2.5, 1.5, 0.1);
    const material = new THREE.MeshPhysicalMaterial({
        map: windowTexture,
        color: 0xffffff,
        transmission: 0.3,
        thickness: 1,
        roughness: 0.6,
        clearcoat: 1.0,
        clearcoatRoughness: 0.2,
        metalness: 0.3,
        transparent: true,
        envMapIntensity: 3});
    const window = new THREE.Mesh(geometry, material);
    //enableShadows(window);
    window.position.set(xPosition, yPosition + 2.5, zPosition -4);
    cena.add(window);
    return window;
}

function createTextSign(text, posx, posy, posz) {
    const loader = new FontLoader();
    loader.load( '/node_modules/three/examples/fonts/helvetiker_bold.typeface.json', function ( font ) {
        const geometry = new THREE.TextGeometry( text, {
            font: font,
            size: 0.5,
            height: 0.1,
            curveSegments: 12,
            bevelEnabled: false,
        } );
        const material = new THREE.MeshPhongMaterial({ color: 0xffffff });
        const sign = new THREE.Mesh(geometry, material);
        sign.position.set(posx, posy, posz);
        cena.add(sign);
    });
}

function CreateMoeAll(x, y, z){
createBuilding(x, y, z);
createDoor(x, y, z);
createWindow(x-3, y, z);
createWindow(x+3, y, z);
createSign(x, y, z);
createTextSign('Moe\'s', x, y + 3.5, z);
const moesBar = createMoesBar();
moesBar.position.set(x-5, y+5, z-4);
cena.add(moesBar);
createlateralWindow(x+5, y+2.5, z+0);
createlateralWindow(x+5, y+2.5, z+2);
createlateralWindow(x+5, y+2.5, z-2);
createlateralWindow(x-5, y+2.5, z+0);
createlateralWindow(x-5, y+2.5, z+2);
createlateralWindow(x-5, y+2.5, z-2);
//createTextSign('Moe\'s', x, y + 3.5, z);
//camara.position.set(x+10, y+8, z-20);
}
//


//codigo do labirinto
let maze = [
[1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
[1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1],
[1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1],
[1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1],
[1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1],
[1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1],
[1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1],
[1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1],
[1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1],
[1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1],
[1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1],
[1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1],
[1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1],
[1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
[1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1],
[1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1],
[1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1],
[1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1],
[1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1],
[1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
[1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
];
/*let maze =  [
    [1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
    [1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1],
    [1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1],
    [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1],
    [1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1],
    [1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1],
    [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  ];*/
  //Inicializar o labirinto: 
        const geometry = new THREE.BoxGeometry(4, 6, 4);
        const textureBC = textureLoader.load('./Wall/Grass_002_COLOR.jpg');
        const textureNM = textureLoader.load('./Wall/Grass_002_NRM.jpg');
        const textureHM = textureLoader.load('./Wall/Grass_002_DISP.png');
        //const textureRM = textureLoader.load('./Wall/Grass_002_COLOR.jpg');
        const textureAOCC = textureLoader.load('./Wall/Grass_002_OCC.jpg');
        //const textureMTLC = textureLoader.load('./Wall/Grass_002_COLOR.jpg');
        const box1Texture = textureLoader.load('/boxes/crate1.png');
        const box2Texture = textureLoader.load('/boxes/crate2.png');

  
    for (let z = 0; z < 21 ; z++) {
      for (let x = 0; x < maze[z].length; x++) {
        if (maze[z][x] === 1) {
          createBlock((x*4)-40, (z*4)-35, Math.PI / 2);}}}
        function createBlock(x, z, rotation) {
        const geometry = new THREE.BoxGeometry(4, 6, 4);
        const material = new THREE.MeshStandardMaterial(
            {
                map: textureBC,
                normalmap: textureNM,
                displaycementmap: textureHM,
                displaacemmentScale: 0.05,
                aoMap: textureAOCC
            });
      const block = new THREE.Mesh(geometry, material);
      block.position.set(x, 0.5, z);
      block.rotation.y = rotation;
      cena.add(block);
    }
//

function CreatePalmTree(x, y, z) {
    const loader = new GLTFLoader();
    loader.load('/palm_tree/scene.gltf', (gltfScene) => {
        const group = new THREE.Group();
        group.add(gltfScene.scene);
        group.position.set(x, y, z);
        group.scale.set(0.05, 0.05, 0.05);
        cena.add(group);
    });
}

function CreateRock(x, y, z) {
    const loader = new GLTFLoader();
    loader.load('/western_stylised_rock/scene.gltf', (gltfScene) => {
        const group = new THREE.Group();
        group.add(gltfScene.scene);
        group.position.set(x, y, z);
        group.scale.set(0.01, 0.01, 0.01);
        cena.add(group);
    });
}

function generatePalmTrees() {
    const treeCount = 50;
    const minDistance = 4;

    let generatedTrees = 0;

    while (generatedTrees < treeCount) {
        let x, z;
        do {
            x = Math.floor(Math.random() * 201) - 100;  // generate x between -100 to 100
            z = Math.floor(Math.random() * 201) - 100;  // generate z between -100 to 100
        } while ((x >= -45 && x <= 45) && (z >= -42 && z <= 70));

        // Check if the position is a valid empty space in the maze
        CreatePalmTree(x, 0, z);
        generatedTrees++;
    }

    
}

function generateRocks() {
    const treeCount = 20;
    const minDistance = 4;

    let generatedRocks = 0;

    while (generatedRocks < treeCount) {
        let x, z;
        do {
            x = Math.floor(Math.random() * 201) - 100;  // generate x between -100 to 100
            z = Math.floor(Math.random() * 201) - 100;  // generate z between -100 to 100
        } while ((x >= -45 && x <= 45) && (z >= -42 && z <= 70));

        // Check if the position is a valid empty space in the maze
        CreateRock(x, 0, z);
        generatedRocks++;
    }

    
}

let stars = [];
let animateStar = false; // This flag will control whether we animate the star or not

function createStar(x, y, z){
    const loader = new GLTFLoader();
    loader.load('/gold_star/scene.gltf', (gltfScene) => {
        let star = new THREE.Group();
        star.add(gltfScene.scene);
        star.position.set(x, y, z);
        star.scale.set(2, 2, 2);
        cena.add(star);
        stars.push(star); // add the star to the stars array
    });
}


function animateestrela() {
    if (animateStar) { // If animateStar is true, we animate the stars
        for (let star of stars) {
            star.rotation.x += 0.01;
            star.rotation.y += 0.01;
            if(star.position.y > 5) {
                star.position.y -= 0.01;
            }
        renderer.render(cena, activeCamera);
        requestAnimationFrame(animateestrela);
    }
}
}

let finale = 0;

function checkFinale() {
    if (finale === 2) {
        animateStar = true; // Start the animation if 'finale' is 1
        animateestrela();
    }
}




function createBox1(x,z) {
    var geometry = new THREE.BoxGeometry(1, 1, 1);
  
      // Create a material using the texture
      const material = new THREE.MeshPhongMaterial({ map: box1Texture });
  
      // Combine the geometry and material into a mesh
      var box = new THREE.Mesh(geometry, material);
      box.position.set(x,1,z);
      // Add the box to the scene
      cena.add(box);
}

function createBox2(x,z) {
    var geometry = new THREE.BoxGeometry(1, 1, 1);
  
      // Create a material using the texture
      const material = new THREE.MeshPhongMaterial({ map: box2Texture });
  
      // Combine the geometry and material into a mesh
      var box = new THREE.Mesh(geometry, material);
      box.position.set(x,1,z);
      // Add the box to the scene
      cena.add(box);
}

function createLightBox(x,z) {
    const pointLight = new THREE.PointLight(0xffffff, 5,10,1);
    pointLight.position.set(x, 1, z);
    cena.add(pointLight);
}

function createspotLightBox(x,z){
    
        const spotLight = new THREE.SpotLight(0xffffff, 1, 100, Math.PI / 4, 0.5, 2);
        spotLight.position.set(x, 4, z);
        spotLight.castShadow = true;
        cena.add(spotLight);
    
        const targetObject = new THREE.Object3D();
        targetObject.position.set(x, 0, z);
        cena.add(targetObject);
    
        spotLight.target = targetObject;
    
}

function createLightBar() {
    const spotLight = new THREE.SpotLight(0xffffff, 2, 100, Math.PI / 4, 0.5, 2);
    spotLight.position.set(-12, 30, 50);
    spotLight.castShadow = true;
    cena.add(spotLight);

    const targetObject = new THREE.Object3D();
    targetObject.position.set(-12, 0, 65);
    cena.add(targetObject);

    spotLight.target = targetObject;
}



var camara = new THREE.OrthographicCamera(-1, 1, 1, -1, 0, -10, 10);
var camaraPerspetiva = new THREE.PerspectiveCamera(45, 4 / 3, 0.1, 225);

const controls = new PointerLockControls(camaraPerspetiva, renderer.domElement)
controls.addEventListener('lock', function(){
});
controls.addEventListener('unlock', function(){
});
document.addEventListener(
    'click',
    function(){
        controls.lock()
    },
    false
);


let activeLight;
let amlight1;
let amlight2;
let activeCamera;
let cam2;
let lastCameraSwitchTime = 0;

let countdown;

function startCountdown(seconds) {
    clearInterval(countdown); // Clear any existing countdown

    const now = Date.now();
    const then = now + seconds * 1000;

    countdown = setInterval(() => {
        const secondsLeft = Math.round((then - Date.now()) / 1000);

        // Check if we should stop the countdown
        if (secondsLeft < 0) {
            clearInterval(countdown);
            document.getElementById('timer').textContent = '';
            return;
        }

        // Display the countdown timer
        document.getElementById('timer').textContent = secondsLeft + 's';

    }, 1000);
}


function switchLight() {
    // remove the currently active light from the scene
    cena.remove(activeLight);

    // switch the active light to the other light
    activeLight = (activeLight === amlight1) ? amlight2 : amlight1;

    // add the new active light to the scene
    cena.add(activeLight);
}

function switchCamera() {
    // switch the active camera to the other camera
    activeCamera = (activeCamera === camaraPerspetiva) ? cam2 : camaraPerspetiva;

    // switch the active light
    switchLight();

    // After 5 seconds, switch back to the original camera and light
    setTimeout(() => {
        activeCamera = (activeCamera === camaraPerspetiva) ? cam2 : camaraPerspetiva;
        switchLight();
    }, 5000); // 5000 milliseconds = 5 seconds
}

function createCameraAnimation() {
    // Create an array of Vector3 points for the curve
    let points = [
      new THREE.Vector3(-3, 30, -51),
      new THREE.Vector3(-3, 22, -43),
      new THREE.Vector3(-3, 2, -42),
    ];
  
    // Create a CatmullRomCurve3 with our points
    let curve = new THREE.CatmullRomCurve3(points);
  
    // Choose the number of points based on the length of your animation
    let divisions = 500;
  
    // Get a set of points along the curve
    let curvePoints = curve.getPoints(divisions);
  
    // Create a new camera and set its initial position
    let animationCamera = new THREE.PerspectiveCamera(
      75,
      window.innerWidth / window.innerHeight,
      0.1,
      1000
    );
    animationCamera.position.copy(curvePoints[0]);
  
    // Set up the animation
    let index = 0;
    let animation = function () {
      if (index < divisions) {
        // Move the camera to the next point along the curve
        animationCamera.position.copy(curvePoints[index]);
  
        // Point the camera at a fixed point while moving down
        if (index > divisions / 2) {
          animationCamera.lookAt(new THREE.Vector3(-3, -1, -42));
        } else {
          animationCamera.lookAt(curvePoints[index + 1]);
        }
  
        // Use the animationCamera to render the scene
        renderer.render(cena, animationCamera);
  
        index++;
  
        // Schedule the next frame
        requestAnimationFrame(animation);
      } else {
        switchLight();
        activeCamera = camaraPerspetiva;
        musicaCrash();
      }
    };
  
    // Start the animation
    animation();
}  
  


function Start() {
    cena.add(meshCubo);
    CreateMoeAll(-12, 0, 65);
    createLightBar();
    CreateCrash(-3, 0, -42);


    amlight1 = new THREE.AmbientLight('#ffffff', 0.2);
    amlight2 = new THREE.AmbientLight('#ffffff', 0.8);

    amlight1.position.y = 10;
    amlight1.position.z = 0;
    amlight2.position.y = 10;
    amlight2.position.z = 0;

    camaraPerspetiva.position.y = 20;
    camaraPerspetiva.position.z = 0;
    camaraPerspetiva.position.x = 0;

    meshCubo.position.z = -55;
    camaraPerspetiva.lookAt(meshCubo.position);

    cam2 = new THREE.OrthographicCamera(-1,1,1,-1,0,-10,10);

// Set the camera position and look at the center of the maze
    cam2.position.set(0, 100, 0);
    cam2.lookAt(0, 0, 0);


    activeCamera = camaraPerspetiva;

    createStar(-12,20,60);
    createStar(-10,20,60);
    createStar(-14,20,60);

    generatePalmTrees();
    generateRocks();


    //trocar a box pelas canecas
    createBox1(4, -10);
    createBox2(-6, 33);
    createLightBox(4,-10);
    createLightBox(-6,33);
    createspotLightBox(4,-10);
    createspotLightBox(-6,33);


    //caixas do labirinto
    createBox1(15, -4);
    createBox2(14, -4);
    createBox1(14, -2);
    createBox2(-4, -2);
    createBox1(-4, 10);
    createBox2(11, 16);
    createBox1(17, 40);
    CreateRock(-16, 0, -20);
    CreateRock(-18, 0, -3);

    activeLight = amlight1; // initially set amlight1 as the active light
    cena.add(activeLight);

    window.addEventListener('keydown', function(event) {
        if (event.key === 'l' || event.key === 'L') {
            switchLight();
        }
    });
    window.addEventListener('keydown', function(event) {
        const currentTime = Date.now(); 
        if (event.key === 'c' || event.key === 'C') {
            if (currentTime - lastCameraSwitchTime >= 40000) {
                switchCamera();
                lastCameraSwitchTime = currentTime; 
                startCountdown(45); // start the countdown of 40 seconds
            }
        }
    });
    
    window.addEventListener('keydown', function(event) {
        if (event.key === 'p' || event.key === 'P') {
            console.log("P key pressed");  // Add this line
            createCameraAnimation();
        }
    });
    


    

    /*window.addEventListener('keydown', function(event) {
        if (event.key === 'p' || event.key === 'P') {
            animateStar = !animateStar; // If "p" is pressed, we toggle the animateStar flag
            if (animateStar) { // If we just set animateStar to true, we start the animation
                animateestrela();
            }
        }
    });*/
    renderer.render(cena, activeCamera);
    requestAnimationFrame(loop);
}

function loop() {

    renderer.render(cena, activeCamera);

    requestAnimationFrame(loop);
}


