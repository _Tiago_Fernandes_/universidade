import { OrbitControls } from 'https://threejs.org/examples/jsm/controls/OrbitControls.js';


function initOrbitControls(camera, renderer) {
    const controls = new OrbitControls(camera, renderer.domElement);
    controls.enableDamping = true; // Ativa o amortecimento (movimento inercial)
    controls.dampingFactor = 0.05; // Fator de amortecimento
    controls.screenSpacePanning = false; // Desativa o movimento horizontal/vertical da câmera
    controls.minDistance = 5; // Distância mínima do alvo
    controls.maxDistance = 20; // Distância máxima do alvo
    controls.maxPolarAngle = Math.PI / 2; // Ângulo máximo de elevação
    return controls;
}


const camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 100);
camera.position.set(0, 5, 15);

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap;
document.body.appendChild(renderer.domElement);

const controls = initOrbitControls(camera, renderer);

