const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

const envTextureLoader = new THREE.CubeTextureLoader();
const environmentMap = envTextureLoader.load([
  'url_da_textura_px', // Positivo X
  'url_da_textura_nx', // Negativo X
  'url_da_textura_py', // Positivo Y
  'url_da_textura_ny', // Negativo Y
  'url_da_textura_pz', // Positivo Z
  'url_da_textura_nz'  // Negativo Z
]);



scene.background = environmentMap;

const textureLoader = new THREE.TextureLoader();

const buildingTexture = textureLoader.load('assets/moe_wall.png', function (texture) {
    texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
    texture.repeat.set(4, 4)});
//const awningTexture = textureLoader.load('assets/AwningRGB.png');
const windowTexture = textureLoader.load('assets/moe_window.png');
const doorTexture = textureLoader.load('assets/moe_door.png');

function createLight() {
    const ambientLight = new THREE.AmbientLight(0xffffff, 0.5);
    scene.add(ambientLight);
  
    const pointLight = new THREE.PointLight(0xffffff, 1);
    pointLight.position.set(10, 10, 10);
    scene.add(pointLight);
}

function createBuilding() {
    const geometry = new THREE.BoxGeometry(10, 5, 8);
    const material = new THREE.MeshPhongMaterial({ map: buildingTexture });
    const building = new THREE.Mesh(geometry, material);
    building.position.set(0, 2.5, 0);
    scene.add(building);
    return building;
}

 
const fontLoader = new THREE.FontLoader();

function createNeonText(text, fontUrl, color) {
    return new Promise((resolve) => {
      fontLoader.load(fontUrl, function (font) {
        const textGeometry = new THREE.TextGeometry(text, {
          font: font,
          size: 1,
          height: 0.2,
        });
  
        const textMaterial = new THREE.MeshBasicMaterial({
          color: color,
          emissive: color,
          emissiveIntensity: 1.5,
        });
  
        const neonText = new THREE.Mesh(textGeometry, textMaterial);
        resolve(neonText);
      });
    });
}



function createMoesBarShape() {
  const shape = new THREE.Shape();
  shape.moveTo(0, 0);
  shape.lineTo(10, 0);
  shape.lineTo(10, 1);
  shape.lineTo(9, 2);
  shape.lineTo(1, 2);
  shape.lineTo(0, 1);
  shape.lineTo(0, 0);
  return shape;
}

function createMoesBarGeometry(shape) {
  const extrudeSettings = {
    steps: 1,
    depth: 0.5,
    bevelEnabled: false,
  };
  return new THREE.ExtrudeGeometry(shape, extrudeSettings);
}

function createMoesBar() {
  const shape = createMoesBarShape();
  const geometry = createMoesBarGeometry(shape);
  const material = new THREE.MeshPhongMaterial({ color: 0x3d294a });
  const moesBar = new THREE.Mesh(geometry, material);
 
  return moesBar;
}


function createSign() {
    const geometry = new THREE.BoxGeometry(3, 1.5, 0.1);
    const material = new THREE.MeshPhongMaterial({ color: 0xbe98cb });
    const sign = new THREE.Mesh(geometry, material);
    sign.position.set(0, 3.5, -4.01);
    scene.add(sign);
    return sign;
}



function createDoor() {
    const geometry = new THREE.BoxGeometry(1.5, 2.5, 0.1);
    const material = new THREE.MeshPhongMaterial({ map: doorTexture });
    const door = new THREE.Mesh(geometry, material);
    door.position.set(0, 1.25, -4);
    scene.add(door);
    return door;
}

function createlateralWindow(xposition, yposition, zposition) {
  const geometry = new THREE.BoxGeometry(0.1, 1.5, 2);
  const material = new THREE.MeshPhysicalMaterial({
      map: windowTexture,
      color: 0xffffff,
      transmission: 0.3,
      thickness: 1,
      roughness: 0.6,
      clearcoat: 1.0,
      clearcoatRoughness: 0.2,
      metalness: 0.3,
      transparent: true,
      envMap: environmentMap,
      envMapIntensity: 3});
  const window = new THREE.Mesh(geometry, material);
  //enableShadows(window);
  window.position.set(xposition, yposition, zposition);
  scene.add(window);
  return window;
}


function createWindow(xPosition) {
    const geometry = new THREE.BoxGeometry(2.5, 1.5, 0.1);
    const material = new THREE.MeshPhysicalMaterial({
        map: windowTexture,
        color: 0xffffff,
        transmission: 0.3,
        thickness: 1,
        roughness: 0.6,
        clearcoat: 1.0,
        clearcoatRoughness: 0.2,
        metalness: 0.3,
        transparent: true,
        envMap: environmentMap,
        envMapIntensity: 3});
    const window = new THREE.Mesh(geometry, material);
    //enableShadows(window);
    window.position.set(xPosition, 2.5, -4);
    scene.add(window);
    return window;
}
  



createLight();
createBuilding();
//createAwning();
createNeonText("MOE'S", "https://threejs.org/examples/fonts/helvetiker_regular.typeface.json", 0x000000, { fontWeight: 1000 }).then((neonText) => {
    neonText.position.set(1.4, 3, -4);
    neonText.rotation.y = Math.PI
    neonText.scale.set(0.7,1,1);
    scene.add(neonText);
  });
createDoor();
createWindow(-3);
createWindow(3);
createSign();

const windowY = 3.5;
const windowSpacing = 2;



const moesBar = createMoesBar();
moesBar.position.set(-5, 5, -4);
scene.add(moesBar);

createlateralWindow(5, 2.5, 0);
createlateralWindow(5,2.5,2);
createlateralWindow(5,2.5,-2);

createlateralWindow(-5, 2.5, 0);
createlateralWindow(-5,2.5,2);
createlateralWindow(-5,2.5,-2);

camera.position.set(10, 8, -20);
camera.lookAt(new THREE.Vector3(0, 4, 0));

function animate() {
  requestAnimationFrame(animate);
  renderer.render(scene, camera);
}
/*function animate() {
  requestAnimationFrame(animate);

  controls.update(); // Atualiza os OrbitControls

  renderer.render(scene, camera);
}*/


animate();
