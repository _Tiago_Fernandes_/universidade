// Inicialização
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75, window.innerWidth/window.innerHeight, 0.1, 1000);
var renderer = new THREE.WebGLRenderer();

renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

// Criação da Caixa
var geometry = new THREE.BoxGeometry(1, 1, 1); // Dimensões da caixa
var material = new THREE.MeshBasicMaterial({color: 0x00ff00}); // Cor da caixa
var cube = new THREE.Mesh(geometry, material); // Criação do objeto
scene.add(cube); // Adicionando o objeto na cena

// Posicionamento da câmera
camera.position.set(0, 8, -10);
camera.lookAt(new THREE.Vector3(0, 4, 0));

// Função de renderização
function animate() {
  requestAnimationFrame(animate);
  /*
  // Rotação da caixa
  cube.rotation.x += 0.01;
  cube.rotation.y += 0.01;*/
  
  // Renderização da cena
  renderer.render(scene, camera);
}
animate();
