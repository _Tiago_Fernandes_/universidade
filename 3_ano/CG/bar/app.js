import { FBXLoader} from 'FBXLoader';

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);


const envTextureLoader = new THREE.CubeTextureLoader();


const textureLoader = new THREE.TextureLoader();

const buildingTexture = textureLoader.load('assets/moe_wall.png', function (texture) {
    texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
    texture.repeat.set(4, 4)});
//const awningTexture = textureLoader.load('assets/AwningRGB.png');
const windowTexture = textureLoader.load('assets/moe_window.png');
const doorTexture = textureLoader.load('assets/moe_door.png');
const boxTexture = textureLoader.load('assets/crate1.png');


function createLight() {

    //Pequena luz de ambiente
    const ambientLight = new THREE.AmbientLight(0xffffff, 0.5);
    scene.add(ambientLight);
  

    //Para mostrar o labirinto de cima também podemos usar para abrir mapa
    /*const pointLight = new THREE.PointLight(0xffffff, 50);
    pointLight.position.set(0, 20, 0);
    scene.add(pointLight);*/

    //create a directional light for the building
    /*const directionalLight = new THREE.DirectionalLight(0xffffff, 1);
    directionalLight.position.set(-12, 0, -250);
    scene.add(directionalLight);*/
  
    //create a spotlight for the building
    //também podemos usar para mostrar partes do mapa
    /*const spotLight = new THREE.SpotLight(0xffffff, 2);
    spotLight.position.set(0, 20, 20);
    scene.add(spotLight);*/

    /*const spotLightHelper = new THREE.SpotLightHelper(spotLight);
    scene.add(spotLightHelper);*/


    //Pequena luz no hemisferio
    const hemisphereLight = new THREE.HemisphereLight(0xffffff, 0x000000, 1);
    hemisphereLight.position.set(0, 50, 0);
    scene.add(hemisphereLight);
}



function createBuilding(posx, posy, posz) {
    const geometry = new THREE.BoxGeometry(10, 5, 8);
    const material = new THREE.MeshPhongMaterial({ map: buildingTexture });
    const building = new THREE.Mesh(geometry, material);
    building.position.set(posx, posy+2.5, posz);
    scene.add(building);
    return building;
}

 
const fontLoader = new THREE.FontLoader();

function createNeonText(text, fontUrl, color) {
    return new Promise((resolve) => {
      fontLoader.load(fontUrl, function (font) {
        const textGeometry = new THREE.TextGeometry(text, {
          font: font,
          size: 1,
          height: 0.2,
        });
  
        const textMaterial = new THREE.MeshBasicMaterial({
          color: color,
          emissive: color,
          emissiveIntensity: 1.5,
        });
  
        const neonText = new THREE.Mesh(textGeometry, textMaterial);
        resolve(neonText);
      });
    });
}



function createMoesBarShape() {
  const shape = new THREE.Shape();
  shape.moveTo(0, 0);
  shape.lineTo(10, 0);
  shape.lineTo(10, 1);
  shape.lineTo(9, 2);
  shape.lineTo(1, 2);
  shape.lineTo(0, 1);
  shape.lineTo(0, 0);
  return shape;
}

function createMoesBarGeometry(shape) {
  const extrudeSettings = {
    steps: 1,
    depth: 0.5,
    bevelEnabled: false,
  };
  return new THREE.ExtrudeGeometry(shape, extrudeSettings);
}

function createMoesBar() {
  const shape = createMoesBarShape();
  const geometry = createMoesBarGeometry(shape);
  const material = new THREE.MeshPhongMaterial({ color: 0x3d294a });
  const moesBar = new THREE.Mesh(geometry, material);
 
  return moesBar;
}


function createSign(posx, posy, posz) {
    const geometry = new THREE.BoxGeometry(3, 1.5, 0.1);
    const material = new THREE.MeshPhongMaterial({ color: 0xbe98cb });
    const sign = new THREE.Mesh(geometry, material);
    sign.position.set(posx + 0, posy + 3.5, posz -4.01);
    scene.add(sign);
    return sign;
}



function createDoor(posx, posy, posz) {
    const geometry = new THREE.BoxGeometry(1.5, 2.5, 0.1);
    const material = new THREE.MeshPhongMaterial({ map: doorTexture });
    const door = new THREE.Mesh(geometry, material);
    door.position.set(posx + 0, posy + 1.25, posz -4);
    scene.add(door);
    return door;
}

function createlateralWindow(xposition, yposition, zposition) {
  const geometry = new THREE.BoxGeometry(0.1, 1.5, 2);
  const material = new THREE.MeshPhysicalMaterial({
      map: windowTexture,
      color: 0xffffff,
      transmission: 0.3,
      thickness: 1,
      roughness: 0.6,
      clearcoat: 1.0,
      clearcoatRoughness: 0.2,
      metalness: 0.3,
      transparent: true,
      });
  const window = new THREE.Mesh(geometry, material);
  //enableShadows(window);
  window.position.set(xposition, yposition, zposition);
  scene.add(window);
  return window;
}


function createWindow(xPosition, yPosition, zPosition) {
    const geometry = new THREE.BoxGeometry(2.5, 1.5, 0.1);
    const material = new THREE.MeshPhysicalMaterial({
        map: windowTexture,
        color: 0xffffff,
        transmission: 0.3,
        thickness: 1,
        roughness: 0.6,
        clearcoat: 1.0,
        clearcoatRoughness: 0.2,
        metalness: 0.3,
        transparent: true,
        envMapIntensity: 3});
    const window = new THREE.Mesh(geometry, material);
    //enableShadows(window);
    window.position.set(xPosition, yPosition + 2.5, zPosition -4);
    scene.add(window);
    return window;
}
  


function CreateMoeAll(x, y, z){
createLight();
createBuilding(x, y, z);

createNeonText("MOE'S", "https://threejs.org/examples/fonts/helvetiker_regular.typeface.json", 0x000000, { fontWeight: 1000 }).then((neonText) => {
    neonText.position.set(1.4, 3, -4);
    neonText.rotation.y = Math.PI
    neonText.scale.set(0.7,1,1);
    scene.add(neonText);
  });
createDoor(x, y, z);
createWindow(x-3, y, z);
createWindow(x+3, y, z);
createSign(x, y, z);



const moesBar = createMoesBar();
moesBar.position.set(x-5, y+5, z-4);
scene.add(moesBar);

createlateralWindow(x+5, y+2.5, z+0);
createlateralWindow(x+5, y+2.5, z+2);
createlateralWindow(x+5, y+2.5, z-2);

createlateralWindow(x-5, y+2.5, z+0);
createlateralWindow(x-5, y+2.5, z+2);
createlateralWindow(x-5, y+2.5, z-2);

camera.position.set(x+10, y+8, z-20);
camera.lookAt(new THREE.Vector3(x-10, y-4, z+20));

function animate() {
  requestAnimationFrame(animate);
  renderer.render(scene, camera);
}
/*function animate() {
  requestAnimationFrame(animate);

  controls.update(); // Atualiza os OrbitControls

  renderer.render(scene, camera);
}*/


animate();
}


/*function createPalm(){
var trunkGeometry = new THREE.CylinderGeometry(0.4, 0.4, 4, 32 );
var trunkMaterial = new THREE.MeshBasicMaterial({ color: 0x8B4513 });
var trunk = new THREE.Mesh(trunkGeometry, trunkMaterial);
scene.add(trunk);

var leavesMaterial = new THREE.MeshBasicMaterial({ color: 0x008000 });
for (var i = 0; i < 8; i++) {
  var leavesGeometry = new THREE.SphereGeometry(2, 1, 1);
  var leaves = new THREE.Mesh(leavesGeometry, leavesMaterial);
  leaves.position.y = 2;
  leaves.position.x = Math.sin(i) * 0.3;
  leaves.position.z = Math.cos(i) * 0.3;
  trunk.add(leaves);
}
trunk.position.set(7, 0, 0);
}*/

// First, you need to create a palm. The palm generator constructor has the following signature:
//let palm = new PalmGenerator(leafGeometry, trunkGeometry, options, curve=false);

// leafGeometry. Required. Has to be an instance of THREE.Geometry, and it is the geometry used for the leaves of the palm.
// trunkGeometry. Required. Has to be an instance of THREE.Geometry, and it is the geometry used for the trunk of the palm.
// options. Optional. Is an object containing the options.

function createBox(x,y,z) {
  var geometry = new THREE.BoxGeometry(1, 1, 1);

    // Create a material using the texture
    const material = new THREE.MeshPhongMaterial({ map: boxTexture });

    // Combine the geometry and material into a mesh
    var box = new THREE.Mesh(geometry, material);
    box.position.set(x,y,z);
    // Add the box to the scene
    scene.add(box);
}


/*function createRock(x, y, z) {
  importer.load('assets/sandstone-rock-desert/source/rock_15/rock_15.fbx', function (object){
    object.scale.x = 0.1;
    object.scale.y = 0.1;
    object.scale.z = 0.1;

    object.position.x=x;
    object.position.y=y;
    object.position.z=z;
    objetoImportado = object;
    scene.add(object);
}) ;
}
*/

CreateMoeAll(0, 0, 0);
createBox(6,0,0);
//createRock(7,0,0);