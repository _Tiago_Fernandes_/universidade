import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:app_teste/app/views/ColabList.dart';
import 'package:app_teste/app/views/viewlinhas.dart';

class ColabLinhas extends StatefulWidget {
  @override
  _ColabLinhasState createState() => _ColabLinhasState();
}

class _ColabLinhasState extends State<ColabLinhas> {
  static const platform = MethodChannel('wear_os_buttons');

  @override
  void initState() {
    super.initState();
    platform.setMethodCallHandler((call) async {
      if (call.method == 'onWearOsButton2') {
        Navigator.pop(context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ViewColaboradores()),
              );
            },
            child: Padding(
              padding: EdgeInsets.only(right: 8.0),
              child: Icon(
                Icons.person,
                color: Color(0xFFFFFFFF),
                size: 40.0,
              ),
            ),
          ),
          Text(
            'Colaboradores',
            style: TextStyle(
              color: Color(0xFFFFA414),
              fontSize: 24.0,
            ),
          ),
          SizedBox(height: 5),
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ViewLinhas()),
              );
            },
            child: Padding(
              padding: EdgeInsets.only(right: 8.0),
              child: Icon(
                Icons.list,
                color: Color(0xFFFFFFFF),
                size: 40.0,
              ),
            ),
          ),
          Text(
            'Linhas',
            style: TextStyle(
              color: Color(0xFFFFA414),
              fontSize: 24.0,
            ),
          ),
        ],
      ),
    );
  }
}