import 'package:flutter/material.dart';

class ProblemIndicator extends StatelessWidget {
  final int index;

  ProblemIndicator({Key? key, required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _buildProblemIndicator(index);
  }

  Widget _buildProblemIndicator(int index) {
    String text1 = '';
    String text2 = '';

    switch (index % 4) {
      case 0:
        text1 = 'Sem';
        text2 = 'Problemas';
        break;
      case 1:
        text1 = 'Problema';
        text2 = 'Leve';
        break;
      case 2:
        text1 = 'Problema';
        text2 = 'Médio';
        break;
      case 3:
        text1 = 'Problema';
        text2 = 'Grave';
        break;
    }

    if (index == 1) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(text1, style: TextStyle(fontSize: 12, color: Colors.white)),
          Text(text2, style: TextStyle(fontSize: 12, color: Colors.white)),
        ],
      );
    }
    if (index == 2) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(text1, style: TextStyle(fontSize: 12, color: Colors.yellow)),
          Text(text2, style: TextStyle(fontSize: 12, color: Colors.yellow)),
        ],
      );
    }
    if (index == 3) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(text1, style: TextStyle(fontSize: 12, color: Colors.red)),
          Text(text2, style: TextStyle(fontSize: 12, color: Colors.red)),
        ],
      );
    }

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(text1, style: TextStyle(fontSize: 12, color: Colors.green)),
        Text(text2, style: TextStyle(fontSize: 12, color: Colors.green)),
      ],
    );
  }
}
