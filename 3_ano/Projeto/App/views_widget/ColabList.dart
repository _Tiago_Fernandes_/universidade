import 'package:flutter/material.dart';



class ViewColaboradores extends StatelessWidget {
  const ViewColaboradores({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 0,
              child: SizedBox(
                width: double.infinity,
                child: Card(
                  color: Colors.black12,
                  child: Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text('Colaboradores',
                                  style: TextStyle(
                                      fontSize: 19,
                                      color: Colors.orangeAccent.shade400)),
                              IconButton(
                                iconSize: 5,
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                icon: Icon(Icons.arrow_back_ios,
                                    color: Colors.orangeAccent.shade400,
                                    size: 20),
                              )
                            ],
                          ),
                        ],
                      )),
                ),
              ),
            ),
            Expanded(
              flex: 10,
              child: ListView.separated(
                shrinkWrap: true,
                itemBuilder: ((context, index) {
                  return SizedBox(
                    height: 30,
                    child: Center(
                      child: ListTile(
                        title: Text('Colaborador $index',
                            style: const TextStyle(
                                fontSize: 18, color: Colors.white)),
                      ),
                    ),
                  );
                }),
                separatorBuilder: (_, __) => const Divider(),
                itemCount: 10,
              ),
            ),
          ],
        ),
      ),
    );
  }
}