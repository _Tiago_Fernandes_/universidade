import 'package:flutter/material.dart';
import 'app/views/ColabLinhas.dart';

void main() {
  runApp(AppWidget());
}

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      home: Scaffold(
        body: ColabLinhas(),
      ),
    );
  }
}

