import 'package:flutter/material.dart';

class ProblemIndicator extends StatelessWidget {
  final int index;

  ProblemIndicator({Key? key, required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _buildProblemIndicator(index);
  }

  Widget _buildProblemIndicator(int index) {
    switch (index % 4) {
      case 0:
        return _buildProblemIndicatorTexts('Sem', 'Problemas', Colors.white);
      case 1:
        return _buildProblemIndicatorTexts('Problema', 'Leve', Colors.yellow);
      case 2:
        return _buildProblemIndicatorTexts('Problema', 'Médio', Colors.yellow);
      case 3:
        return _buildProblemIndicatorTexts('Problema', 'Grave', Colors.red);
    }

    return _buildProblemIndicatorTexts('Sem', 'Problemas', Colors.white);
  }

  Widget _buildProblemIndicatorTexts(String text1, String text2, Color color) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(text1, style: TextStyle(fontSize: 12, color: color)),
        Text(text2, style: TextStyle(fontSize: 12, color: color)),
      ],
    );
  }
}