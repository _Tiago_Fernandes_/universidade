import 'package:flutter/material.dart';
import 'dart:math';
import 'package:app_teste/app/Widgets/BuildProblem.dart';



class ViewLinhas extends StatelessWidget {
  ViewLinhas({Key? key}) : super(key: key);

  var random = Random();
  int num = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 0,
              child: SizedBox(
                width: double.infinity,
                child: Card(
                  color: Colors.black12,
                  child: Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 10),
                              child: Text(
                                'Linhas',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.orangeAccent.shade400),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 50),
                              child: IconButton(
                                iconSize: 5,
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                icon: Icon(Icons.arrow_back_ios,
                                    color: Colors.orangeAccent.shade400,
                                    size: 20),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 10,
              child: ListView.separated(
                shrinkWrap: true,
                itemBuilder: ((context, index) {
                  return SizedBox(
                    height: 35,
                    child: ListTile(
                      title: Text('Linha $index',
                          style: const TextStyle(
                              fontSize: 16, color: Colors.white)),
                      trailing: ProblemIndicator(index:random.nextInt(4)),
                    ),
                  );
                }),
                separatorBuilder: (_, __) => const Divider(),
                itemCount: 10,
              ),
            ),
          ],
        ),
      ),
    );
  }
}