﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

class Program
{
    [STAThread]
    static void Main(string[] args)
    {
        // Prompt the user to enter the IP address of the server
        Console.Write("Please enter the IP address of the server: ");
        string serverIp = Console.ReadLine();

        // Create a TcpClient object
        TcpClient client = new TcpClient();
        client.Connect(IPAddress.Parse(serverIp), 8888);
        Console.WriteLine("Conectado ao servidor.");

        NetworkStream stream = client.GetStream();

        // Receive response from the server
        byte[] data = new byte[1024];
        int bytesRead = stream.Read(data, 0, data.Length);
        string response = Encoding.ASCII.GetString(data, 0, bytesRead);
        Console.WriteLine("Server response: {0}", response);

        try
        {
            // Loop principal do cliente
            while (true)
            {
                Console.WriteLine("Digite 'SEND' para enviar um arquivo .csv, 'QUIT' para sair:");
                string input = Console.ReadLine();

                if (input.ToUpper() == "QUIT")
                {
                    // Enviar comando de saída ao servidor
                    byte[] message = Encoding.ASCII.GetBytes("QUIT");
                    stream.Write(message, 0, message.Length);

                    // Receive the response from the server
                    StreamReader rea = new StreamReader(client.GetStream());
                    string response_quit = rea.ReadLine();
                    Console.WriteLine("Server response: " + response_quit);
                    Console.WriteLine("Desconectado do servidor.");

                    break;
                }
                else if (input.ToUpper() == "SEND")
                {
                    // Solicitar o caminho do arquivo .csv a ser enviado
                    Console.WriteLine("Selecione o arquivo .csv a ser enviado:");
                    Thread.Sleep(1000);

                    // Adicione este bloco para abrir a janela de diálogo do Windows
                    OpenFileDialog openFileDialog = new OpenFileDialog
                    {
                        InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                        Filter = "Ficheiros CSV (*.csv)|*.csv",
                        FilterIndex = 1,
                        RestoreDirectory = true
                    };

                    string filePath;
                    if (openFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        filePath = openFileDialog.FileName;
                    }
                    else
                    {
                        Console.WriteLine("Nenhum ficheiro selecionado. A aplicação será encerrada.");
                        Thread.Sleep(2000);
                        return;
                    }

                    // Ler o arquivo em bytes
                    string fileContent = File.ReadAllText(filePath);
                    string fileName = Path.GetFileName(filePath);

                    // Enviar o comando, o nome do arquivo e o conteúdo do arquivo ao servidor
                    string messageToSend = $"SEND {fileName}{Environment.NewLine}{fileContent}";
                    byte[] message = Encoding.ASCII.GetBytes(messageToSend);
                    stream.Write(message, 0, message.Length);

                    Console.WriteLine($"Arquivo '{fileName}' enviado com sucesso para o servidor.");

                    data = new byte[1024];
                    bytesRead = stream.Read(data, 0, data.Length);
                    response = Encoding.ASCII.GetString(data, 0, bytesRead);
                    Console.WriteLine("Server response: {0}", response);
                }
                else
                {
                    Console.WriteLine("Comando inválido. Por favor, digite 'SEND' para enviar um arquivo .csv ou 'QUIT' para sair.");
                }
            }

            // Fechar aconexão com o servidor
            stream.Close();
            client.Close();
        }
        catch (Exception ex)
        {
            Console.WriteLine("Ocorreu um erro: " + ex.Message);
        }
    }
}
