import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Register')),
      body: ListView(
        children: [
          Center(
            child: Text(
              'MyApp',
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
          ),
          TextField(
            decoration: InputDecoration(labelText: 'Email'),
          ),
          TextField(
            obscureText: true,
            decoration: InputDecoration(labelText: 'Password'),
          ),
          TextField(
            obscureText: true,
            decoration: InputDecoration(labelText: 'Confirm Password'),
          ),
          TextField(
            keyboardType: TextInputType.number,
            decoration: InputDecoration(labelText: 'Phone Number'),
          ),
          ElevatedButton(
              child: Text('Register'), onPressed: () {/* Register User */})
        ],
      ),
    );
  }
}
