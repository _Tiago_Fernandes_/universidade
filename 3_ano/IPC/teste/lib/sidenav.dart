import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home Screen"),
      ),
      drawer: Drawer(
        backgroundColor: Color.fromRGBO(89, 89, 89, 1.0),
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            SizedBox(height: 50),
            SizedBox(
              child: ListTile(
                  leading: Icon(
                      color: Color.fromRGBO(255, 255, 255, 1),
                      Icons.account_circle_outlined,
                      size: 80),
                  title: Text('User Name',
                      style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 1),
                        fontSize: 30,
                      )),
                  subtitle: Text(
                    'Edit Profile',
                    style: TextStyle(
                      color: Color.fromRGBO(252, 139, 6, 1),
                      fontSize: 15,
                    ),
                  ),
                  onTap: () {
                    // Navigate to edit profile
                  }),
            ),
            SizedBox(
              height: 30,
            ),
            SizedBox(
                height: 20,
                child: Container(
                  color: Color.fromRGBO(255, 255, 255, 1),
                )),
            SizedBox(height: 25),
            ListTile(
              title: Text('Payement',
                  style: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1),
                    fontSize: 25,
                  )),
              leading: Icon(
                color: Color.fromRGBO(255, 255, 255, 1),
                Icons.credit_card,
                size: 45,
              ),
              onTap: () {
                // Update the state of the app.
                // ...
              },
            ),
            ListTile(
              title: Text('My Trips',
                  style: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1),
                    fontSize: 25,
                  )),
              leading: Icon(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  Icons.lock_clock,
                  size: 45),
              onTap: () {
                // Update the state of the app.
                // ...
              },
            ),
            SizedBox(height: 300),
            ListTile(
              title: Text('Support',
                  style: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1),
                    fontSize: 25,
                  )),
              leading: Icon(
                color: Color.fromRGBO(255, 255, 255, 1),
                Icons.help,
                size: 45,
              ),
              onTap: () {
                // Update the state of the app.
                // ...
              },
            )
          ],
        ),
      ),
      body: Center(
        child: Text("Home Screen Contents"),
      ),
    );
  }
}
