import 'package:flutter/material.dart';
import './perfil.dart';
import 'Payment.dart';
import './Travels.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController _controller = TextEditingController();
  List<String> _filteredItems = [];
  final List<String> _listItems =
      List<String>.generate(10, (index) => 'Destino $index');
  String? selectedItem;
  bool isItemSelected = false;
  bool isExpanded = false;


  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Não foi possível abrir o link: $url';
    }
    }

  @override
  void initState() {
    _controller.addListener(() {
      setState(() {
        _filteredItems = _listItems
            .where((item) =>
                item.toLowerCase().contains(_controller.text.toLowerCase()))
            .toList();
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: [
          GestureDetector(
            onTap: () {
              setState(() {
                isItemSelected = false;
                isExpanded = false;
                selectedItem = null;
              });
            },
            child: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/map.png"),
                  fit: BoxFit.cover,
                ),
              ),
              child: isItemSelected
                  ? Container()
                  : ListView.builder(
                      itemCount: _listItems.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTile(
                          title: Text(_listItems[index]),
                          onTap: () {
                            setState(() {
                              selectedItem = _listItems[index];
                              isItemSelected = true;
                              isExpanded = true;
                            });
                          },
                        );
                      },
                    ),
            ),
          ),
          Positioned(
            top: 50.0,
            left: 30.0,
            child: FloatingActionButton(
              backgroundColor: const Color(0xFF414141), // your custom color
              child: const Icon(
                Icons.menu,
                color: Colors.white, // white icon
              ),
              onPressed: () {
                _scaffoldKey.currentState!.openDrawer();
              },
            ),
          ),
        ],
      ),
      drawer: Drawer(
        backgroundColor: const Color.fromRGBO(89, 89, 89, 1.0),
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const SizedBox(height: 50),
            SizedBox(
              child: ListTile(
                  leading: const Icon(
                      color: Color.fromRGBO(255, 255, 255, 1),
                      Icons.account_circle_outlined,
                      size: 80),
                  title: const Text('Nome do Usuário',
                      style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 1),
                        fontSize: 30,
                      )),
                  subtitle: const Text(
                    'Editar Perfil',
                    style: TextStyle(
                      color: Color.fromRGBO(252, 139, 6, 1),
                      fontSize: 15,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const InfoPerfil()),
                    );
                  }),
            ),
            const SizedBox(
              height: 30,
            ),
            SizedBox(
                height: 2,
                child: Container(
                  color: const Color.fromRGBO(255, 255, 255, 1),
                )),
            const SizedBox(height: 25),
            ListTile(
              title: const Text('Pagamento',
                  style: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1),
                    fontSize: 25,
                  )),
              leading: const Icon(
                color: Color.fromRGBO(255, 255, 255, 1),
                Icons.credit_card,
                size: 45,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const PaymentPage()),
                );
              },
            ),
            ListTile(
              title: const Text('Minhas Viagens',
                  style: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1),
                    fontSize: 25,
                  )),
              leading: const Icon(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  Icons.lock_clock,
                  size: 45),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const TravelsPage()),
                );
              },
            ),
            const SizedBox(height: 420),
            ListTile(
              title: const Text('Suporte',
                  style: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1),
                    fontSize: 25,
                  )),
              leading: const Icon(
                color: Color.fromRGBO(255, 255, 255, 1),
                Icons.help,
                size: 45,
              ),
              onTap: () {
                _launchURL('https://www.apd.org.pt');
              },
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: ExpansionTile(
          initiallyExpanded: isExpanded,
          onExpansionChanged: (bool expanding) =>
              setState(() => isExpanded = expanding),
          title: TextField(
            controller: _controller,
            decoration: const InputDecoration(
              hintText: 'Local de Destino',
              hintStyle: TextStyle(color: Colors.white54),
              filled: false,
              border: InputBorder.none,
            ),
            style: const TextStyle(color: Colors.white),
          ),
          children: <Widget>[
            if (selectedItem != null)
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Image(
                      image: AssetImage('assets/kindpng_612906.png'),
                      width: 150,
                      height: 150,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 40.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text('Destino : $selectedItem'),
                          const Padding(
                            padding: EdgeInsets.only(left: 40.0),
                            child: Text('Preço: 10€'),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ElevatedButton(
                            onPressed: () {
                              setState(() {
                                isItemSelected = false;
                                isExpanded = false;
                                selectedItem = null;
                              });
                            },
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.deepOrange,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50),
                              ),
                            ),
                            child: const Text(
                              'Comprar',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: OutlinedButton(
                            onPressed: () {
                              setState(() {
                                isItemSelected = false;
                                isExpanded = false;
                                selectedItem = null;
                              });
                            },
                            child: const Text(
                              'Cancelar',
                              style: TextStyle(
                                color: Colors.deepOrange,
                              ),
                            ),
                            style: OutlinedButton.styleFrom(
                              side: BorderSide(
                                  color: Colors.deepOrange, width: 2),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50),
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            if (isExpanded && !isItemSelected)
              ..._listItems
                  .where((item) => item
                      .toLowerCase()
                      .contains(_controller.text.toLowerCase()))
                  .map((item) => ListTile(
                        title: Text(item),
                        onTap: () {
                          setState(() {
                            selectedItem = item;
                            isItemSelected = true;
                            isExpanded = true;
                          });
                        },
                      ))
                  .toList(),
          ],
        ),
      ),
    );
  }
}
