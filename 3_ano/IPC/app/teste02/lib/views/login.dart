import 'package:flutter/material.dart';
import './HomePage.dart';
import './register.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/image.jpg'), fit: BoxFit.cover)),
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(height: 200),
            const Text(
              'Ride4ALL',
              style: TextStyle(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  fontSize: 70,
                  fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 50),
            TextField(
              enabled: true,
              decoration: InputDecoration(
                  labelText: 'E-Mail',
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(28),
                      borderSide: const BorderSide(
                          color: Color.fromRGBO(255, 255, 255, 1), width: 2)),
                  hintStyle:
                      const TextStyle(color: Color.fromRGBO(255, 255, 255, 1)),
                  hintText: "E-Mail"),
            ),
            const SizedBox(height: 20),
            Semantics(
              label: 'Palavra-passe',
              child: TextField(
                obscureText: true,
                enabled: true,
                decoration: InputDecoration(
                    labelText: 'Palavra-passe',
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(28),
                        borderSide: const BorderSide(
                            color: Color.fromRGBO(255, 255, 255, 1), width: 2)),
                    hintStyle: const TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 1)),
                    hintText: "Palavra-passe"),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const SizedBox(
                  height: 150,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const RegisterPage()),
                    );
                  },
                  child: const Text('Registar',
                      style: TextStyle(
                          decoration: TextDecoration.underline,
                          fontSize: 20,
                          color: Color.fromRGBO(255, 255, 255, 1))),
                ),
                SizedBox(
                    width: 250,
                    height: 45,
                    child: ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const HomePage()),
                          );
                        },
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(28)),
                            backgroundColor:
                                const Color.fromRGBO(252, 139, 6, 1)),
                        child: const Text('Entrar',
                            style: TextStyle(
                                color: Color.fromRGBO(30, 30, 30, 1))))),
              ],
            )
          ],
        ),
      ),
    );
  }
}
