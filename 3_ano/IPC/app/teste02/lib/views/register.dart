import 'package:flutter/material.dart';
import 'login.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/image.jpg'), fit: BoxFit.cover)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(height: 50),
            IconButton(
              icon: const Icon(Icons.account_circle),
              iconSize: 120,
              color: const Color.fromRGBO(255, 255, 255, 1),
              onPressed: () {},
            ),
            const SizedBox(height: 20),
            const Text(
              'Ride4All',
              style: TextStyle(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  fontSize: 50,
                  fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 20),
            Center(
                child: SizedBox(
                    width: 375,
                    child: TextField(
                      enabled: true,
                      decoration: InputDecoration(
                        labelText: 'E-Mail',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(28),
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                width: 2)),
                        filled: true,
                        hintStyle:
                            const TextStyle(color: Color.fromRGBO(255, 255, 255, 1)),
                        hintText: "E-Mail",
                      ),
                    ))),
            const SizedBox(height: 20),
            Center(
                child: SizedBox(
                    width: 375,
                    child: TextField(
                      obscureText: true,
                      enabled: true,
                      decoration: InputDecoration(
                          labelText: 'Palavra-passe',
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(28),
                              borderSide: const BorderSide(
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                  width: 2)),
                          hintStyle: const TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1)),
                          hintText: "Palavra-passe"),
                    ))),
            const SizedBox(height: 20),
            Center(
                child: SizedBox(
                    width: 375,
                    child: TextField(
                      obscureText: true,
                      enabled: true,
                      decoration: InputDecoration(
                        labelText: 'Confirmar Palavra-passe',
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(28),
                              borderSide: const BorderSide(
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                  width: 2)),
                          filled: true,
                          hintStyle: const TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1)),
                          hintText: "Confirmar Palavra-passe"),
                    ))),
            const SizedBox(height: 20),
            Center(
                child: SizedBox(
                    width: 375,
                    child: TextField(
                      enabled: true,
                      decoration: InputDecoration(
                        labelText: 'Numero de Telemovel',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(28),
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                width: 2)),
                        filled: true,
                        hintStyle:
                            const TextStyle(color: Color.fromRGBO(255, 255, 255, 1)),
                        hintText: "Numero de Telemovel",
                      ),
                    ))),
            const SizedBox(height: 20),
            Center(
                child: SizedBox(
                    width: 250,
                    height: 45,
                    child: ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const LoginPage()));
                        },
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(28)),
                            backgroundColor: const Color(0xFFFF9800)),
                        child: const Text('Registar',
                            style: TextStyle(
                                color: Color.fromRGBO(30, 30, 30, 1)))))),
          ],
        ),
      ),
    );
  }
}
