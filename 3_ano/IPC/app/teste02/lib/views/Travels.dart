import 'package:flutter/material.dart';

class TravelsPage extends StatefulWidget {
  const TravelsPage({super.key});

  @override  
  _TravelsPageState createState() => _TravelsPageState();
}

class _TravelsPageState extends State<TravelsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orangeAccent,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pop(context);    
          },
        ),
        title: const Text(
          'Historico de Viagens',
          style: TextStyle(
            color: Colors.black,                  
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Container(     
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/image2.jpg'),              
            fit: BoxFit.cover,
          ),              
        ),     
        child: Column(
          children: [
            const SizedBox(height: 20), 
            const Padding(  
              padding: EdgeInsets.only(left: 16),              
              child: Text(
                'MES-ANO',
                style: TextStyle(
                  color: Colors.white,                            
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Expanded(            
              child: ListView.builder(  
                itemCount: 4,                  
                itemBuilder: (context, index) {                            
                  return Container(
                    height: 50,                   
                    margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),  
                    child: ListTile(
                      title: Text('Viagem $index'),
                      subtitle: const Row(                        
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [                           
                            Text('Distancia-duracao'),                      
                            Text('custo'),
                      ],),
                    ),
                  );        
                },
              ),      
            ),
          ],
        ),  
      ),   
    );
  }
}