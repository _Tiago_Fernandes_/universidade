import 'package:flutter/material.dart';

class PaymentPage extends StatefulWidget {
  const PaymentPage({super.key});

  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  bool _creditCardChecked = true;
  bool _debitCardChecked = false;

  // Controllers para os campos de entrada
  final _cardNumberController = TextEditingController();
  final _expiryDateController = TextEditingController();
  final _cvvController = TextEditingController();

  @override
  void dispose() {
    // Limpe os controllers quando o widget for descartado
    _cardNumberController.dispose();
    _expiryDateController.dispose();
    _cvvController.dispose();
    super.dispose();
  }

  void _addCard() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Adicionar novo cartão'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                TextField(
                  controller: _cardNumberController,
                  decoration: InputDecoration(hintText: 'Número do cartão'),
                  keyboardType: TextInputType.number,
                ),
                TextField(
                  controller: _expiryDateController,
                  decoration: InputDecoration(hintText: 'Validade (MM/AA)'),
                ),
                TextField(
                  controller: _cvvController,
                  decoration: InputDecoration(hintText: 'CVV'),
                  keyboardType: TextInputType.number,
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Adicionar'),
              onPressed: () {
                // Implementar a lógica de adicionar o cartão aqui.
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orangeAccent,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: const Text(
          ' Pagamento',
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/image2.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 30),
            const Row(
              children: [
                SizedBox(width: 20,),
                Icon(Icons.history, color: Colors.white),
                SizedBox(height: 10),
                Text(
                  ' Historico de Compra',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            const SizedBox(height: 20),
            ListView(
              shrinkWrap: true,
              children: [
                const SizedBox(height: 20),
                const SizedBox(width: 20),
                const Text(
                  ' Metodos de pagamento',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 20),
                ListTile(
                  leading: Checkbox(
                    value: _creditCardChecked,
                    onChanged: (value) {
                      setState(() {
                        _creditCardChecked = value!;
                        _debitCardChecked = !value;
                      });
                    },
                  ),
                  title: const Row(
                    children: [
                      Icon(Icons.payment, color: Colors.white),
                      SizedBox(width: 10),
                      Text(
                        'Cartão de crédito',
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  trailing: const Text(
                    '1234',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                ListTile(
                  leading: Checkbox(
                    value: _debitCardChecked,
                    onChanged: (value) {
                      setState(() {
                        _debitCardChecked = value!;
                        _creditCardChecked = !value;
                      });
                    },
                  ),
                  title: const Row(
                    children: [
                      Icon(Icons.payment, color: Colors.white),
                      SizedBox(width: 10),
                      Text(
                        'Cartao de debito',
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
          ),
                  trailing: const Text(
                    '5678',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                // Add more ListTiles for each payment method
              ],
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _addCard,
        child: Icon(Icons.add),
      ),
    );
  }
}