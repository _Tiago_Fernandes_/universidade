import 'package:flutter/material.dart';
import './register.dart';
import './sidenav.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/image.jpg'), fit: BoxFit.cover)),
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 200),
            Text(
              'Ride4ALL',
              style: TextStyle(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  fontSize: 70,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 50),
            TextFormField(
              enabled: false,
              decoration: InputDecoration(
                  disabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(28),
                      borderSide: BorderSide(
                          color: Color.fromRGBO(255, 255, 255, 1), width: 2)),
                  hintStyle: TextStyle(color: Color.fromRGBO(255, 255, 255, 1)),
                  hintText: "E-Mail"),
            ),
            SizedBox(height: 20),
            TextField(
              enabled: false,
              decoration: InputDecoration(
                  disabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(28),
                      borderSide: BorderSide(
                          color: Color.fromRGBO(255, 255, 255, 1), width: 2)),
                  hintStyle: TextStyle(color: Color.fromRGBO(255, 255, 255, 1)),
                  hintText: "Password"),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  height: 150,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => RegisterPage()),
                    );
                  },
                  child: Text('Sign In',
                      style: TextStyle(
                          decoration: TextDecoration.underline,
                          fontSize: 20,
                          color: Color.fromRGBO(255, 255, 255, 1))),
                ),
                SizedBox(
                    width: 250,
                    height: 45,
                    child: ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HomeScreen()),
                          );
                        },
                        child: Text('Login',
                            style: TextStyle(
                                color: Color.fromRGBO(30, 30, 30, 1))),
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(28)),
                            backgroundColor: Color.fromRGBO(252, 139, 6, 1)))),
              ],
            )
          ],
        ),
      ),
    );
  }
}
