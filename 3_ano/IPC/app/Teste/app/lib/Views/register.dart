import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/image.jpg'), fit: BoxFit.cover)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 50),
            IconButton(
              icon: Icon(Icons.account_circle),
              iconSize: 120,
              color: Color.fromRGBO(255, 255, 255, 1),
              onPressed: () {},
            ),
            SizedBox(height: 20),
            Text(
              'Ride4All',
              style: TextStyle(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  fontSize: 50,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 20),
            Center(
                child: SizedBox(
                    width: 375,
                    child: TextField(
                      enabled: false,
                      decoration: InputDecoration(
                        disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(28),
                            borderSide: BorderSide(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                width: 2)),
                        filled: true,
                        hintStyle:
                            TextStyle(color: Color.fromRGBO(255, 255, 255, 1)),
                        hintText: "E-Mail",
                      ),
                    ))),
            SizedBox(height: 20),
            Center(
                child: SizedBox(
                    width: 375,
                    child: TextField(
                      enabled: false,
                      decoration: InputDecoration(
                          disabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(28),
                              borderSide: BorderSide(
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                  width: 2)),
                          hintStyle: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1)),
                          hintText: "Password"),
                    ))),
            SizedBox(height: 20),
            Center(
                child: SizedBox(
                    width: 375,
                    child: TextField(
                      enabled: false,
                      decoration: InputDecoration(
                          disabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(28),
                              borderSide: BorderSide(
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                  width: 2)),
                          filled: true,
                          hintStyle: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1)),
                          hintText: "Confirm Password"),
                    ))),
            SizedBox(height: 20),
            Center(
                child: SizedBox(
                    width: 375,
                    child: TextField(
                      enabled: false,
                      decoration: InputDecoration(
                        disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(28),
                            borderSide: BorderSide(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                width: 2)),
                        filled: true,
                        hintStyle:
                            TextStyle(color: Color.fromRGBO(255, 255, 255, 1)),
                        hintText: "Number",
                      ),
                    ))),
            SizedBox(height: 20),
            Center(
                child: SizedBox(
                    width: 250,
                    height: 45,
                    child: ElevatedButton(
                        onPressed: () {},
                        child: Text('Sign Up',
                            style: TextStyle(
                                color: Color.fromRGBO(30, 30, 30, 1))),
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(28)),
                            backgroundColor: Color(0xFFFF9800))))),
          ],
        ),
      ),
    );
  }
}
