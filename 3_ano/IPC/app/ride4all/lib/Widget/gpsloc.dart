import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';

class GPSWidget extends StatefulWidget {
  @override
  _GPSWidgetState createState() => _GPSWidgetState();
}

class _GPSWidgetState extends State<GPSWidget> {
  final MapController mapController;
  late Future<Position> currentPosition;

    _GPSWidgetState() : mapController = MapController();

  @override
  void initState() {
    super.initState();
    mapController = MapController();
    currentPosition = _getLocation();
  }

  Future<Position> _getLocation() async {
    return await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Position>(
      future: currentPosition,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return Center(child: Text('Error: ${snapshot.error}'));
        } else {
          Position position = snapshot.data!;
          mapController.move(LatLng(position.latitude, position.longitude), 15);
          return FlutterMap(
            mapController: mapController,
            options: MapOptions(
              center: LatLng(position.latitude, position.longitude),
              zoom: 15.0,
            ),
            layers: [
              TileLayerOptions(
                urlTemplate: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                subdomains: ['a', 'b', 'c'],
              ),
              MarkerLayerOptions(markers: [
                Marker(
                  width: 80.0,
                  height: 80.0,
                  point: LatLng(position.latitude, position.longitude),
                  builder: (ctx) => Icon(
                    Icons.location_pin,
                    color: Colors.red,
                    size: 40.0,
                  ),
                ),
              ]),
            ],
          );
        }
      },
    );
  }
}
