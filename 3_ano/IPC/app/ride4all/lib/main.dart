import 'package:flutter/material.dart';
import 'Widget/gpsloc.dart';

void main() {
  runApp(
    MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text('GPS Widget with Flutter Map')),
        body: GPSWidget(),
    ),
    ),
  );
}
