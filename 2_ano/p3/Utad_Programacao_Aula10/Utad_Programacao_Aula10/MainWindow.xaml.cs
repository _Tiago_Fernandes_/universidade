﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Utad_Programacao_Aula10
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Variável com apontador para App (Camada de Interligação)
        private App app;
        public MainWindow()
        {
            InitializeComponent();

            //Obtencao do apontador para App (Camada de Interligacao)
            app = App.Current as App;

            //Subscrição de método da View em Event do Model (Interligação Model->View)
            app.M_Bateria.CargaAlterada += M_Bateria_CargaAlterada;
        }

        private void M_Bateria_CargaAlterada(int carga)
        {
            //Atualiza estado da aplicação do Model (Interligação View->Model)
            pbBateria.Value = carga;
            this.Title = "Gestor da Bateria - " + carga + "%";
        }

        private void btnCarregar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Invocação de método do Model (Interligação View->Model)
                app.M_Bateria.Carregar();
            }
            catch(OperacaoInvalidaException erro)
            {
                MessageBox.Show(erro.Message, "Erro", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnDescarregar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Invocação de método do Model (Interligação View->Model)
                app.M_Bateria.Descarregar();
            }
            catch(OperacaoInvalidaException erro)
            {
                MessageBox.Show(erro.Message, "Erro", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
