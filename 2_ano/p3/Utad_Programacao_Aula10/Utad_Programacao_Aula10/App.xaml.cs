﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Utad_Programacao_Aula10
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        //Models
        public ModelBateria M_Bateria { get; private set; }

        public App()
        {
            //Inicializacao dos models
            M_Bateria= new ModelBateria();
        }
    }
}
