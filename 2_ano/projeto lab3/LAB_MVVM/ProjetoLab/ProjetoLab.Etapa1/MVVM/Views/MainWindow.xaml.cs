﻿using ProjetoLab.Etapa1.MVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjetoLab.Etapa1
{
 
    public partial class MainWindow : Window
    {

        private readonly ViewModel viewmodel;
        
        public MainWindow()
        {
            InitializeComponent();
            viewmodel = new ViewModel();
            DataContext = viewmodel ;
        }


      
    }
}
