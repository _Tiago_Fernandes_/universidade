﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoLab.Etapa1.Models
{
    internal class Lista
    {
        private string Descricao { get; set; }

        public void CriarLista() { } //pode ser Constructor??

        public void ApagarLista() { }

        public void AlterarLista() { }   

        public void AdicionarItem(Produto item) { }

        public void RemoverItem(Produto item) { }

        public void EditarItem(Produto item) { }

        public void ComprarItem(Produto item) { }
    }
}
