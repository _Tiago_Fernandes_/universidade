﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoLab.Etapa1.Models
{
    internal class Produto
    {
        private string Descricao { get; set; }
        private string Quantidade { get; set; }
        private string Categoria { get; set; } // referenciar as pre-defenidas
        private bool Comprado { get; set; }
       
    }
}
