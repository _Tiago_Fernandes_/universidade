﻿using mvvmtest.MVVM.ViewModels;
using System.Windows;

namespace mvvmtest
{
    public partial class MainWindow : Window
    {
        private readonly MyViewModel _viewModel;

        public MainWindow()
        {
            InitializeComponent();
            _viewModel = new MyViewModel();
            // The DataContext serves as the starting point of Binding Paths
            DataContext = _viewModel;
        }
    }
}
